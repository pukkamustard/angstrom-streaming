let () =
  let filename1 = Sys.argv.(1) in
  let filename2 = Sys.argv.(2) in
  let on_parse pp msg = Format.printf "got a (streamed) message: %a@." pp msg in
  let on_parse_msg = on_parse Message.pp in
  let on_parse_rec_type = on_parse Rec_type.pp in
  let () = Fmt.pr "------ parsing using character sequence@." in
  let open Angstrom_streaming.File_char in
  let () = Fmt.pr "--- file 1@." in
  parse Message.Parser.message filename1 on_parse_msg;
  let () = Fmt.pr "--- file 2@." in
  parse Rec_type.Parser.c' filename2 on_parse_rec_type;
  let () = Fmt.pr "------ parsing using string sequence@." in
  let open Angstrom_streaming.File_string in
  let () = Fmt.pr "--- file 1@." in
  parse Message.Parser.message filename1 on_parse_msg;
  let () = Fmt.pr "--- file 2@." in
  parse Rec_type.Parser.c' filename2 on_parse_rec_type;
