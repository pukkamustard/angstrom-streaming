(** Examples of strings, which can be parsed as messages, are
    'abcdef 3 .'
    'abcdefghijkl 5.4 .'
*)

type message = Message_int of string * int | Message_float of string * float

type t = message

module Parser = struct
  open Angstrom

  let message_int = lift2
      (fun s i -> Message_int (s, i))
      (Parse_util.take_until ' ' <* char ' ')
      (Angstrom.any_int8 <* char ' ' <* char '.')

  let message_float = lift2
      (fun s f -> Message_float (s, f))
      (Parse_util.take_until ' ')
      (char ' ' *> Parse_util.small_float <* char ' ' <* char '.')

  let message = (message_int <|> message_float) <* (many (char '\n'))
end

let pp ppf = function
  | Message_int (s, i) -> Fmt.pf ppf "%s %d" s i
  | Message_float (s, f) -> Fmt.pf ppf "%s %f" s f


