open Angstrom

let take_until s = take_while (fun c -> Char.equal s c |> not)

(** Parses from [-255.255, 255.255] *)
let small_float = lift2
    (fun a b -> Float.of_string @@ (string_of_int a) ^ "." ^ (string_of_int b))
    any_int8
    (char '.' *> any_int8)

let delimiters c1 c2 p =
  char c1 *> p <* char c2

let delimiters_string_not_greedy c1 c2 = delimiters c1 c2 @@
  take_while (fun d -> not @@ Char.equal c2 d)
