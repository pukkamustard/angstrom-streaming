(* @future hard-coded for simplicity *)
let buf_size = 0x1000

module Parse_generic = struct
  let handle_parse_result state =
    let open Angstrom.Buffered in
    match state_to_unconsumed state with
    | None    -> assert false
    | Some us -> us, state_to_result state

  let parse p on_parse = Angstrom.(skip_many (p <* commit >>| on_parse))
end

(* This signature is rougly equal to that of Angstrom-lwt-unix, with the addition of to_seq *)
module type Parse = sig
  type t
  val with_buffered_parse_state : 'a Angstrom.Buffered.state -> t Seq.t -> Angstrom.Buffered.unconsumed * ('a, string) result
  val parse : 'a Angstrom.t -> t Seq.t -> Angstrom.Buffered.unconsumed * ('a, string) result
  val parse_many : 'a Angstrom.t -> ('a -> 'b) -> t Seq.t -> Angstrom.Buffered.unconsumed * (unit, string) result
  val to_seq : string -> t Seq.t
end

module Parse_string : Parse = struct
  type t = string
  let rec buffered_state_loop state seq =
    let open Angstrom.Buffered in
    match state with
    | Partial k ->
      begin match Seq.uncons seq with
        | Some (h, tail) -> k (`String h) |> (fun state' ->
            buffered_state_loop state' tail)
        | None -> k `Eof
      end
    | state -> state

  (** @not-tested Provides more fine-grained control over what happens when parsing
      fails or succeeds compared to parse_many. *)
  let with_buffered_parse_state state seq =
    let open Angstrom.Buffered in
    begin match state with
      | Partial _ -> buffered_state_loop state seq
      | _         -> state
    end
    |> Parse_generic.handle_parse_result

  let parse p in_seq =
    let open Angstrom.Buffered in
    let state = buffered_state_loop (parse p) in_seq in
    Parse_generic.handle_parse_result state

  let parse_many p on_parse in_seq = parse (Parse_generic.parse p on_parse) in_seq
  let to_seq = Sequence.String.of_file
end

module Parse_char : Parse = struct
  type t = char
  let rec buffered_state_loop state seq bytes =
    let open Angstrom.Buffered in
    let size = Bytes.length bytes in
    match state with
    | Partial k ->
      begin match Sequence.input_seq seq bytes size with
        | _seq', 0 -> k `Eof
        (* In the following line, the information that has moved from the sequence to the
           buffer is parsed, using k. The new state' is passed on, and Bytes.sub doesn't
           change bytes itself, so that can be reused (and overwritten) in the following step *)
        | seq', n -> k (`String (Bytes.(unsafe_to_string (sub bytes 0 n)))) |> (fun state' ->
            buffered_state_loop state' seq' bytes)
      end
    | state -> state

  (** @not-tested Provides more fine-grained control over what happens when parsing
      fails or succeeds compared to parse_many. *)
  let with_buffered_parse_state state seq =
    let open Angstrom.Buffered in
    let bytes = Bytes.create buf_size in
    begin match state with
      | Partial _ -> buffered_state_loop state seq bytes
      | _         -> state
    end
    |> Parse_generic.handle_parse_result

  let parse p in_seq =
    let open Angstrom.Buffered in
    let bytes = Bytes.create buf_size in
    let state = buffered_state_loop (parse p) in_seq bytes in
    Parse_generic.handle_parse_result state

  let parse_many p on_parse in_seq = parse (Parse_generic.parse p on_parse) in_seq
  let to_seq = Sequence.Char.of_file
end

(** Module functor which creates the main entry points to the parse functions
 * which can be used for parsing files.
 *
 * The module argument [Parse] abstracts over char/string-based sequences which
 * read from files.
 *
 * @todo allow sequences produced from sources other than files
 *)
module Make (Parse: Parse) = struct
  let run_parser p to_seqable =
    Parse.parse p (Parse.to_seq to_seqable)

  let run_parser_many p to_seqable on_parse =
    Parse.parse_many p on_parse (Parse.to_seq to_seqable)

  let parse_once p to_seqable on_parse =
    let unconsumed, result' = run_parser p to_seqable in
    match result' with
    | Ok result ->
      let () = on_parse result in
      print_endline @@ "num unconsumed chars: " ^ string_of_int unconsumed.len
    | Error s -> print_endline @@ "parse error: " ^ s

  let parse p to_seqable on_parse =
    let unconsumed, result' = run_parser_many p to_seqable on_parse in
    match result' with
    | Ok () ->
      let () = print_endline "finished parsing stream" in
      print_endline @@ "num unconsumed chars: " ^ string_of_int unconsumed.len
    | Error s -> print_endline @@ "stream parse error: " ^ s
end

module File_char = Make (Parse_char)
module File_string = Make (Parse_string)
