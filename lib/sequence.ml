(* the first function is input_seq. It behaves, at least that's the purpose, similar to
   input : in_chan -> bytes -> int -> int -> int
   input ic bts pos len = ...
   A notable difference is that it returns the (tail of the) sequence. In a way, an
   input channel is a mutable value (when you read it, the location changes),
   so you can use it to continue reading the file. For sequences, we have to return
   the part of the sequence that is not read.
*)

(*

We assume that: input_chan: abcdef

We require the following properties:

0) len < 1 : it should fail
1) len = 5 : return = (f, 5) (buffer: abcde)
2) len = 6  : return = (nil, 6) (buffer: abcdef)
3) len = 10 : return = (nil, 6) (buffer: abcdef....)
4) len < length of buffer : e.g.
   len = 5, length of buffer = 8, return = (f, 5) (buffer: abcde...)
5) len > length of buffer : it should fail

*)

let input_seq seq buf len =
  if len < 1 then raise (Invalid_argument "len must be greater than 0") else
  if Bytes.length buf < len then raise (Invalid_argument "len must be smaller than or equal to the length of the buffer") else
    let rec loop seq' i =
      if i = len then
        (seq', i)
      else
        match Seq.uncons seq' with
        | Some (c, tail) -> Bytes.set buf i c; loop tail (i + 1)
        | None -> Seq.empty, i in
    loop seq 0

module type Input = sig
  type t
  val input : in_channel -> t
end

module Make (Input: Input) = struct
  (* This is taken from rdf-xml *)
  let of_file file =
    let ic = open_in file in
    let rec read () =
      try
        let input = Input.input ic in
        Seq.Cons (input, read)
      with
      | End_of_file ->
        close_in ic;
        Seq.Nil
      | e ->
        close_in_noerr ic;
        raise e
    in read
end

(** @todo: this splits on newlines, make generic *)
module String = Make (struct
    type t = string
    let input = input_line
  end)

module Char = Make (struct
    type t = char
    let input = input_char
  end)
